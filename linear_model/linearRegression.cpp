//
// Created by DucDung Nguyen on 05/04/2017.
//

#include "linearRegression.h"

void divide(int &D, int &B, int count, int num_folds)
{
	D = count/num_folds;
	if (count % num_folds == 0)
		B = 0;

	else 
		B = count - D*(num_folds-1);
};

void GD(DataIterator_t& dataIter, int N, ProbParam_t& param, LinearRegressionModel_t& model, int start1, int end1, int start2, int end2)
{
	float X, Y;
	for (int j = 1; j <= param.nIterations; j++)
	{
		X = 0; Y = 0;
		
		for (int i = start1; i < end1; i++)
			X += (model.a*dataIter.data[i].x + model.b - dataIter.data[i].y)*dataIter.data[i].x;

		for (int i = start1; i < end1; i++)
			Y += (model.a*dataIter.data[i].x + model.b - dataIter.data[i].y);
		
		if (start2 != 0)
		{
			for (int i = start2; i < end2; i++)
				X += (model.a*dataIter.data[i].x + model.b - dataIter.data[i].y)*dataIter.data[i].x;
			for (int i = start2; i < end2; i++)
				Y += (model.a*dataIter.data[i].x + model.b - dataIter.data[i].y);
		}
			
		model.a -= param.learningRate*X/(sqrt (X*X + Y*Y));
		model.b -= param.learningRate*Y/(sqrt (X*X + Y*Y));
	}

	cout << setw (7) << model.a << setw(7) << model.b; //print
};

void TRN (DataIterator_t& dataIter, ProbParam_t& param, LinearRegressionModel_t& model, int k)
{
	int D, B;
	divide(D, B, dataIter.data.size(), param.nFolds);
	model.a = param.startPoint.x;
	model.b = param.startPoint.y;

	if (k == 1)	
		GD(dataIter, dataIter.data.size() - D, param, model, D, dataIter.data.size(), 0, 0);

	else if (k > 1 && k < param.nFolds)
		GD(dataIter, dataIter.data.size() - D, param, model, 0, (k-1)*D, k*D, dataIter.data.size());

	else if (k == param.nFolds)
		GD(dataIter, dataIter.data.size() - B, param, model, 0, dataIter.data.size() - B, 0, 0) ;
};

void Ermsd (DataIterator_t& dataIter, LinearRegressionModel_t& model, int start, int Diff)
{
	model.Ermsd = 0;
	for (int i = start; i < start + Diff; i++)
		model.Ermsd += (1.0/Diff)*pow(model.a*dataIter.data[i].x + model.b - dataIter.data[i].y, 2);
	
	model.Ermsd = sqrt(model.Ermsd);

	cout << setw (7) << model.Ermsd; //print
};

void frequency (DataIterator_t& dataIter, ProbParam_t& param, LinearRegressionModel_t& model, vector <float>& Err, int start, int Diff)
{
	float Vmin, Vmax, sigma = 0, e_bar = 0, length, total = 0;
	int j;
	
	for (int i = start; i < start + Diff; i++)
		model.errHistogram.push_back(model.a*dataIter.data[i].x + model.b - dataIter.data[i].y);
	
	for (int i = 0; i < Diff; i++)
		e_bar += (1.0/Diff)*model.errHistogram[i];
		
	for (int i = 0; i < Diff; i++)
		sigma += (1.0/Diff)*pow(model.errHistogram[i]- e_bar, 2);
		
	sigma = sqrt(sigma);
	Vmin = e_bar - 3*sigma; 
	Vmax = e_bar + 3*sigma;
	length = fabs(Vmax - Vmin)/10;
	
	for (int i = 0; i < Diff; i ++)
	{
		j = 0;
		while(j < 10)
		{
			if (model.errHistogram[i] > Vmin + j*length && model.errHistogram[i] < Vmin + (j+1)*length)
			{
				Err[j]++;
				break;
			}
			j++;
		}
	}

	for (int i = 0; i < 10; i++)
		total +=Err[i];
	
	if (total != 0)
		for (int i = 0; i < 10; i++)
			Err[i] = Err[i] / total;

	if (param.eval != 0)
		for (int i = 0; i < 10; i++)
			cout << setw(7) << Err[i]; //print
	cout << endl;

	model.errHistogram.clear();
};

void TST (DataIterator_t& dataIter, ProbParam_t& param, LinearRegressionModel_t& model, int k)
{
	int D, B;
	divide(D, B, dataIter.data.size(), param.nFolds);
	vector <float> Err(10, 0);

	if (k == 1)
	{
		Ermsd(dataIter, model, 0, D);
		frequency(dataIter, param, model, Err, 0, D);
	}

	else if (k < param.nFolds && k > 1)
	{
		Ermsd(dataIter, model, (k-1)*D, D);
		frequency(dataIter, param, model, Err, (k-1)*D, D);
	}

	else if (k == param.nFolds)
		if (B != 0)
		{
			Ermsd(dataIter, model, (k-1)*D, B);
			frequency (dataIter, param, model, Err, (k-1)*D, B);
		}
		else
		{
			Ermsd(dataIter, model, (k-1)*D, D);
			frequency(dataIter, param, model, Err, (k-1)*D, D);
		}
};

bool kFoldTraining(DataIterator_t& dataIter, ProbParam_t& param, LinearRegressionModel_t& model) 
{
    // TODO: Your code goes here
	
	if (dataIter.data.size() < param.nFolds)
		return false;
	else 
	{
		cout << "---------------------------------------------------------------------------------";
		cout << endl;
		cout << "Output of the validation";
		cout << endl;
		cout << "---------------------------------------------------------------------------------";
		cout << endl;

		for (int i = 1; i <= param.nFolds; i++)
		{
			TRN(dataIter, param, model, i);
			TST(dataIter, param, model, i);
		}

		return true;
	}
}
