//
// Created by DucDung Nguyen on 05/04/2017.
//
#include "commonLib.h"

// Load data from the input file
// return: true if loading process is success
bool loadData(char* fName, DataIterator_t& data) {
    // TODO: write your code to read data points from file and put them in vector "data"
	ifstream infile(fName);
	Point2D temp;
	string line;
	int check = 0;
	
	if (infile) 
	{
		while (getline(infile, line)) 
		{	
			if (line[line.length() - 1] == '\r')
				line.erase(line.length() - 1);

			if (line.length() > 0)
			{
				if (check > 2)
				{
					istringstream iss(line);
					iss >> temp.x;
					iss >> temp.y;
					data.data.push_back(temp);
				}

				else if (check == 1 && line != "Data samples") 
					return false;
			}

			if (check < 3)
				check++;
		}
		infile.close();
		return true;
    }

    else 
		return false;
}

// Load parameters from the input file
// return: true if loading process is success
bool loadParams(char* fName, ProbParam_t& param) {
    ifstream infile(fName);
	string line, data;
	int check = 0;
	if (infile) 
	{
		while (getline(infile, line)) 
		{
			if (line[line.length() - 1] == '\r')
                line.erase(line.length() - 1);

			if (line.length() > 0) 
			{
				if (check == 1 && line != "Training and Validation Parameters") 
					return false;
					
				else if (check > 2)
				{
					istringstream iss(line);
					iss >> data;

					if (data == "num_iterations:")
						iss >> param.nIterations;
		
					else if (data == "learning_rate:")
						iss >> param.learningRate;
					
					else if (data == "start_a:")
						iss >> param.startPoint.x;
				
					else if (data ==  "start_b:")
						iss >> param.startPoint.y;
				
					else if (data == "num_folds:")
						iss >> param.nFolds;
			
					else if (data == "eval:")
						iss >> param.eval;
				}
			}

			if (check < 3)
				check++;
			
		}
        infile.close();
		return true;
    }

    else 
		return false;
}
